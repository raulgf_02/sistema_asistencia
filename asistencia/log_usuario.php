
<!DOCTYPE html>
<html>
<head>
	<?php include("../asistencia/header.php");?>
	<style type="text/css">
        body{ font: 14px sans-serif; }
        .wrapper{ width: 350px; padding: 20px; }
    </style>
</head>
<body>
	<?php include("../asistencia/menu.php");?>
	<div class="wrapper">
        <?php 
    include("../asistencia/is_logout.php");
    require_once '../conection/conection.php';
    $msg_error = "";
    $user = $_SESSION['username'];
    $stm = $conn->prepare("CALL sp_getUser(?)");
    $stm->bind_param("s", $user);
    $stm->execute();

    $name = "";
    $apellidos = "";
    $result = $stm->get_result();
    if($result->num_rows === 0) die('No rows');
    while($row = $result->fetch_assoc()) {
        $id = $row['id'];
    }
    //echo $result;
    $stm->close();
    $stm = $conn->prepare("CALL sp_getLogUsario(?)");
    $stm->bind_param("i",$id);
    $stm->execute();
    $result = $stm->get_result();
    if ($result->num_rows === 0){
        echo "<div class='alert alert-primary' role='alert'>
                No hay cambios registrados!
            </div>";
    }
    if ($result->num_rows > 0){
        echo "<h3>Log de Usuario: ".$user."</h3>";
        echo "<table class='table table-bordered table-striped'>";
            echo "<thead>";
                echo "<tr>";
                    echo "<th>#</th>";
                    echo "<th>Observacion</th>";
                    echo "<th>Fecha</th>";
                echo "</tr>";
            echo "</thead>";
            echo "<tbody>";
            while($row = $result->fetch_assoc()){
                $hubo_cambio = false;
                $observacion = "Hubo cambio en: ";
                if ($row['nuevo_nombre'] != $row['ant_nombre']){
                    $hubo_cambio = true;
                    $observacion .= " nombre";
                }
                if($row['nuevo_apellido'] != $row['ant_apellido']){
                    $hubo_cambio = true;
                    $observacion .= " apellido";
                }
                if($row['nuevo_pass'] != $row['ant_pass']){
                    $hubo_cambio = true;
                    $observacion .= " contraseña";
                }
                if ($hubo_cambio){
                    echo "<tr>";
                        echo "<td>" . $row['id'] . "</td>";
                        echo "<td>" . $observacion . "</td>";
                        echo "<td>" . $row['fecha'] . "</td>";
                    echo "</tr>";
                    }
                }
            echo "</tbody>";
        echo "</table>";
        }
 ?>
    </div>  
</body>
</html>