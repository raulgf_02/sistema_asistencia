<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
  <a class="navbar-brand" href="../asistencia/inicio.php">Sistema Asistencia</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="collapsibleNavbar">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link" href="../asistencia/ver_asistencia.php">Ver Asistencia</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="../asistencia/editar_usuario.php">Editar Datos</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="../asistencia/restart_pass.php">Reestablecer contraseña</a>
      </li>    
    </ul>
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" href="../registration/logout.php">Cerrar Sesión <i class="fa fa-power-off"></i></a>
      </li>
    </ul>

  </div>  
</nav>