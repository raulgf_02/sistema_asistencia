<?php 
	include("../asistencia/is_logout.php");
	require_once '../conection/conection.php';
	$user = $_SESSION['username'];
	$stm = $conn->prepare("CALL sp_getUser(?)");
	$stm->bind_param("s", $user);
	$stm->execute();
	$result = $stm->get_result();
	if($result->num_rows === 0) die('No rows');
	while($row = $result->fetch_assoc()) {
		$id = $row['id'];
	}
	$stm->close();
	//Capturando datos actuales
	$stm = $conn->prepare("call sp_get_asistencia(?)");
	$stm->bind_param("s", $id);
	$stm->execute();
	$result = $stm->get_result();
	//Inicializando datos
	$hend = NULL;
	$astart = NULL;
	$aend = NULL;
	$hstart = NULL;
	if($result->num_rows === 0){
		$hstart = NULL;
	}else{
		while($row = $result->fetch_assoc()){
			$hstart = $row['hora_llegada'];
			$hend = $row['hora_salida'];
			$astart = $row['almuerzo_llegada'];
			$aend = $row['almuerzo_salida'];
		}
	}
	$stm->close();
	if($_SERVER["REQUEST_METHOD"] == "POST"){
		date_default_timezone_set('America/Lima');
		//capturar los datos actuales
		$tipo = $_POST["tipo"];
		if ($tipo == "hstart"){
			$hstart = date("Y-m-d H:i:s");
		}
		if ($tipo == "hend"){
			$hend = date("Y-m-d H:i:s");
		}
		if ($tipo == "astart"){
			$astart = date("Y-m-d H:i:s");
		}
		if ($tipo == "aend"){
			$aend = date("Y-m-d H:i:s");
		}
		//echo $tipo;
		$stm = $conn->prepare("CALL sp_asistenciaCreate(?,?,?,?,?)");
		$stm->bind_param("sssss", $id,$hstart,$hend,$astart,$aend);
		$stm->execute();
		$stm->close();
	}

?> 
<!DOCTYPE html>
<html lang="en">
<head>
    <?php include("../asistencia/header.php");?>
    <style type="text/css">
        body{ font: 14px sans-serif; text-align: center; }
        .wrapper{ width: 350px; padding: 20px; }
    </style>
</head>
<body onload="startTime()">
    <?php include("../asistencia/menu.php");?>
    <div class="page-header">
        <h4>Hola, <b><?php echo htmlspecialchars($_SESSION['username']); ?></b>. Bienvenido al sistema de asistencia.</h4>
        <div class="row">
		  <div class="col-sm-offset-1 col-sm-6">
		    <div class="card text-white bg-info mb-3" style="max-width: 20rem;">
		    	<div class="card-header">
		        	Entrada y Salida
		    	</div>
		      <div class="card-body">
		        <p class="card-text">Marcar Hora de llegada</p>
		        <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
		        	<input type="text" class="form-control" name="reloj" id="reloj">
		        	<hr>
		        	<?php
		        		if ($hstart == NULL){
		        			echo "<input type='hidden' name='tipo' value='hstart'>";
		        			echo "<input type='submit' class='btn btn-primary' value='Llegada'>";
		        		}else{
		        			echo "<input type='hidden' name='tipo' value='hend'>";
		        			echo "<input type='submit' class='btn btn-primary' value='Salida'>";
		        		}
		        	 ?>
		    	</form>
		      </div>
		    </div>
		  </div>
		  <div class="col-sm-6">
		    <div class="card text-white bg-dark mb-3" style="max-width: 20rem;">
		    	<div class="card-header">
		        	Almuerzo
		    	</div>
		      <div class="card-body">
		        <p class="card-text">Aqui se marca hora de almuerzo.</p>
		        <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
		        	<input type="text" class="form-control" name="reloj2" id="reloj2">
		        	<hr>
		        	<?php
		        		if ($astart == NULL){
		        			echo "<input type='hidden' name='tipo' value='astart'>";
		        			echo "<input type='submit' class='btn btn-primary' value='Llegada'>";
		        		}else{
		        			echo "<input type='hidden' name='tipo' value='aend'>";
		        			echo "<input type='submit' class='btn btn-primary' value='Salida'>";
		        		}
		        	 ?>
		    	</form>
		      </div>
		    </div>
		  </div>
		</div>
    </div>
    <script type="text/javascript">
    	function startTime() {
		    var today = new Date();
		    var h = today.getHours();
		    var m = today.getMinutes();
		    var s = today.getSeconds();
		    m = checkTime(m);
		    s = checkTime(s);
		    document.getElementById('reloj').value =
		    h + ":" + m + ":" + s;
		    document.getElementById('reloj2').value =
		    h + ":" + m + ":" + s;
		    var t = setTimeout(startTime, 500);
		}
		function checkTime(i) {
		    if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
		    return i;
		}
    </script>
</body>
</html>