<html>
<head>
	<title></title>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
</head>
<body>

	<?php
		require_once '../conection/conection.php';
		session_start();
		$user = $_SESSION['username'];
		//echo $user;
		//Capturando el valor de anio
		if ($_GET["anio"] == ""){
			$anio = NULL;
		}else{
			$anio = intval($_GET["anio"]);
		}
		//Capturando el valor de mes
		if ($_GET["mes"] == ""){
			$mes = NULL;
		}else{
			$mes = intval($_GET["mes"]);
		}
		$stm = $conn ->prepare("SELECT id FROM usuario WHERE user=?");
		$stm->bind_param("s",$user);
		$stm->execute();
		$id = 0;
		$result = $stm->get_result();
		if ($result->num_rows === 0) die('No rows');
		while($row = $result->fetch_row()){
			$id = $row[0];
		}
		$stm->close();
		$stm = $conn->prepare("CALL sp_asistencia_empleado_v2(?,?,?)");
		$stm->bind_param("iii",$id,$anio,$mes);
		$stm->execute();
		$result = $stm->get_result();
		if ($result->num_rows === 0){
			echo "<div class='alert alert-primary' role='alert'>
  					No hay data!
				</div>";
		}
		if ($result->num_rows > 0){
			echo "<table class='table table-bordered table-striped'>";
				echo "<thead>";
					echo "<tr>";
						echo "<th>#</th>";
						echo "<th>Hora Llegada</th>";
						echo "<th>Hora Salida</th>";
						echo "<th>Almuerzo Llegada</th>";
						echo "<th>Almuerzo Salida</th>";
					echo "</tr>";
				echo "</thead>";
				echo "<tbody>";
				while($row = $result->fetch_assoc()){
					echo "<tr>";
						echo "<td>" . $row['id'] . "</td>";
						echo "<td>" . $row['hora_llegada'] . "</td>";
						echo "<td>" . $row['hora_salida'] . "</td>";
						echo "<td>" . $row['almuerzo_llegada'] . "</td>";
						echo "<td>" . $row['almuerzo_salida'] . "</td>";
						echo "</tr>";
					}
				echo "</tbody>";
			echo "</table>";
		}

	 ?>
</body>
</html>