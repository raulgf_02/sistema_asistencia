<html>
<head>  
	<?php include("../asistencia/header.php");?>
    <style type="text/css">
        body{ font: 14px sans-serif; }
        .wrapper{ width: 100%; padding: 20px; }
    </style>
</head>
<body>
    <?php include("../asistencia/menu.php");?>
	<div class="wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="page-header clearfix">
                        <h2 class="pull-left">Asistencia Detalles</h2>
                    </div>
                    <form>
                        <div class="row">
                            <select class="form-control col-sm-3" name="anio" id="anio">
                                <option value="">Año</option>
                                <option value="2018">2018</option>
                                <option value="2017">2017</option>
                                <option value="2016">2016</option>
                            </select>
                            <select class="form-control col-sm-3" name="mes" id="mes">
                                <option value="">Mes</option>
                                <option value="1">Enero</option>
                                <option value="2">Febrero</option>
                                <option value="3">Marzo</option>
                                <option value="4">Abril</option>
                                <option value="5">Mayo</option>
                                <option value="6">Junio</option>
                                <option value="7">Julio</option>
                                <option value="8">Agosto</option>
                                <option value="9">Setiembre</option>
                                <option value="10">Octubre</option>
                                <option value="11">Noviembre</option>
                                <option value="12">Diciembre</option>
                            </select>
                        </div>
                    </form>
                        <button class="btn btn-info btn-xs" id="filtrar">Filtrar</button>
                        <hr />
                        <div id="response"></div>
                </div>
            </div>        
        </div>
    </div>
     <script type="text/javascript">
        function filter(anio,mes){
            if (window.XMLHttpRequest) {
                // code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp=new XMLHttpRequest();
            } else { // code for IE6, IE5
                xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
            }
            console.log(xmlhttp);
            xmlhttp.onreadystatechange=function() {
                console.log(this.readyState);
                //console.log(this.responseText);
                console.log(this.status);
                console.log(this.responseText);
                if (this.readyState==4 && this.status==200) {
                    document.getElementById("response").innerHTML=this.responseText;
                }
            }
            xmlhttp.open("GET","filtros_asistencia.php?anio="+anio+"&mes="+mes,true);
            xmlhttp.send();
        }
        $("#filtrar").click(function(){
            var anio = document.getElementById("anio").value;
            var mes = document.getElementById("mes").value;
            filter(anio,mes);
        });
        $(document).ready(function(){
            //inicializando filtro
            filter("","");
        })
    </script>
</body>
</html>