<?php 
	include("../asistencia/is_logout.php");
	require_once '../conection/conection.php';
	$msg_error = "";
	$user = $_SESSION['username'];
	$stm = $conn->prepare("CALL sp_getUser(?)");
	$stm->bind_param("s", $user);
	$stm->execute();

	$name = "";
	$apellidos = "";
	$result = $stm->get_result();
	if($result->num_rows === 0) die('No rows');
	while($row = $result->fetch_assoc()) {
		$id = $row['id'];
		$name = $row['nombres'];
		$apellidos = $row['apellidos'];
		$password = $row['password'];
	}
	//echo $result;
	$stm->close();
	//Proceso para editar personas 
	if($_SERVER["REQUEST_METHOD"] == "POST"){
		$name = $_POST["nombres"];
		$apellidos = $_POST["apellidos"];
		$stm = $conn->prepare("CALL sp_Updateusuario(?,?,?,?)");
		$stm->bind_param("ssss", $name,$apellidos,$user,$password);
		$stm->execute();
	}
 ?>
<!DOCTYPE html>
<html>
<head>
	<?php include("../asistencia/header.php");?>
	<style type="text/css">
        body{ font: 14px sans-serif; }
        .wrapper{ width: 350px; padding: 20px; }
    </style>
</head>
<body>
	<?php include("../asistencia/menu.php");?>
	<div class="wrapper">
        <h2>Edita tus datos aquí</h2>
        <?php 
        	if ($msg_error != ""){
        		echo "<div class='alert alert-danger'>$msg_error</div>";
        	} 
         ?>
        <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
            <div class="form-group">
                <label>Nombres</label>
                <input type="text" name="nombres" class="form-control" value="<?php echo $name; ?>">
            </div>    
            <div class="form-group">
                <label>Apellidos</label>
                <input type="text" name="apellidos" class="form-control" value="<?php echo $apellidos; ?>">
            </div>
            <div class="form-group">
                <input type="submit" class="btn btn-primary" value="Editar">
                <a href="../asistencia/log_usuario.php" class="btn btn-secondary">Historial</a>
            </div>
        </form>
    </div>  
</body>
</html>