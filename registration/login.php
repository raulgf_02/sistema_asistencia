<?php 
	require_once '../conection/conection.php';
	$msg_error = "";
	if($_SERVER["REQUEST_METHOD"] == "POST"){
		$user = $_POST["username"];
		$pass = $_POST["password"];
		//$user = "kbaldeon";
		//$pass = "apddple";
		$stm = $conn->prepare("SELECT validateLogin(?,?)");
		$stm->bind_param("ss", $user, $pass);
		$stm->execute();

		$result = $stm->get_result();
		if($result->num_rows === 0) die('No rows');
		while($row = $result->fetch_row()) {
			if($row[0] == 1){
				session_start();
				$_SESSION['username'] = $user;
				header("location: ../asistencia/inicio.php");
			}else{
				$msg_error ="Verifique su usuario y contraseña";
			}
		}
		//echo $result;
		$stm->close();
	}

 ?>
<html>
<head>
	<?php include("../asistencia/header.php");?>
	<style type="text/css">
        body{ font: 14px sans-serif; }
        .wrapper{ width: 350px; padding: 20px; }
    </style>
</head>
<body>
	<div class="wrapper">
        <h2>Login</h2>
        <p>Ingresa con tus credenciales.</p>
        <?php 
        	if ($msg_error != ""){
        		echo "<div class='alert alert-danger'>$msg_error</div>";
        	} 
         ?>
        <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
            <div class="form-group">
                <label>Username</label>
                <input type="text" name="username" class="form-control">
            </div>    
            <div class="form-group">
                <label>Password</label>
                <input type="password" name="password" class="form-control">
            </div>
            <div class="form-group">
                <input type="submit" class="btn btn-primary" value="Login">
            </div>
            <p>No tienes cuenta? <a href="registro.html">Registrate</a>.</p>
        </form>
    </div>    
</body>
</html>