<?php 
	$servername = "localhost";
	$username = "root";
	$password = "";
	$dbname = "sistema_asistencia";

	//Creando la conexion
	//$conn = new mysqli($servername, $username, $password, $dbname);

	try {
		
		$conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
		$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		//Capturando los datos
		$nombres = $_REQUEST['nombres'];
		$apellidos = $_REQUEST['apellidos'];
		$user = $_REQUEST['user'];
		$pass = $_REQUEST['pass'];

		//Proceso para crear un nuevo ususario
		$stmt = $conn->prepare("CALL sp_usuarioCreate(:nombres,:apellidos,:user,:password)");

		$stmt->bindParam(':nombres', $nombres);
		$stmt->bindParam(':apellidos', $apellidos);
		$stmt->bindParam(':user', $user);
		$stmt->bindParam(':password', $password);

		$stmt->execute();
		echo "Registrado correctamente";
	} catch (Exception $e) {
		echo "Hubo un error";
	}
?>	